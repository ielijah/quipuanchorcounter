﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WikiGetRandomLink
{
    internal class Program
    {
        /// <summary>
        /// Get Random Links from wiki and put it to app.config
        /// </summary>
        /// <param name="args">First=app.config path second=Count of links</param>
        static void Main(string[] args)
        {
            string path = args.Any()? args[0] : "AnchorCounter.config";
            int count = args.Any() && args.Length>1?int.Parse(args[1]??"100"):100;

            var listUri = GetRandomWikiPages(count);

            string StringifyURL = string.Join(" ", listUri);

            AddOrUpdateAppSettings("URLS", StringifyURL, path);
        }

        /// <summary>
        /// Get Random Pages From Wiki
        /// </summary>
        /// <param name="count">Count of page for return</param>
        /// <returns>List of Uri</returns>
        static ConcurrentBag<Uri> GetRandomWikiPages(int count = 100)
        {
            var uries = new ConcurrentBag<Uri>();
            Random rn = new Random();
            Parallel.ForEach(new int[count], uri =>
            {
                new ParallelOptions
                {
                    MaxDegreeOfParallelism = Convert.ToInt32(Math.Ceiling((Environment.ProcessorCount * 0.75) * 2.0))
                };
                WebRequest request = WebRequest.Create("https://en.wikipedia.org/wiki/Special:Random");
                WebResponse response = request.GetResponse();
                response.Close();

                Console.ForegroundColor = (ConsoleColor)rn.Next(1,16);
                Console.WriteLine($@"GET {response.ResponseUri}");
                uries.Add(response.ResponseUri);
            });
            return uries;
        }
        /// <summary>
        /// Add Or Update config
        /// </summary>
        /// <param name="key">Key of appSettings parameter</param>
        /// <param name="value">New value of appSettings parameter for adding or updating</param>
        /// <param name="path">Path to Config file (AnchorCounter.config by default)</param>
        static void AddOrUpdateAppSettings(string key, string value, string path = "AnchorCounter.config")
        {
            try
            {
                if (!File.Exists(path))
                {
                    var file = File.Create(path);
                    file.Dispose();
                }
                ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "AnchorCounter.config" };
                Configuration configFile = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified, false);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}
