﻿using HtmlAgilityPack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;

namespace QuipuAnchorCounter
{
    class AnchorCounterViewModel : INotifyPropertyChanged
    {
        int _maxAnchorCount = 0;

        ICommand _initAppCommand;

        ICommand _readLinksFromConfigCommand;

        ICommand _updateConfigCommand;

        ICommand _startCountingCommand;

        ICommand _stopCountingCommand;

        bool _canExecute = true;

        bool _canCancel = false;

        CancellationTokenSource cts;

        string _generalState = "Ready!";

        int _checkedLinksCount = 0;

        ObservableCollection<QuipuAnchorItem> quipuAnchorItems;
        public AnchorCounterViewModel()
        {
            InitAppCommand = new RelayCommand(InitApp, param => this.CanExecute);
            ReadLinksFromConfigCommand = new RelayCommand(ReadLinksFromConfig, param => this.CanExecute);
            UpdateConfigCommand = new RelayCommand(UpdateConfig, param => this.CanExecute);
            StartCountingCommand = new RelayCommand(StartCounting, param => this.CanExecute);
            StopCountingCommand = new RelayCommand(StopCounting, param => this.CanCancel);
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


        public bool CanExecute
        {
            get
            {
                return _canExecute;
            }
            set
            {
                _canExecute = value;
                OnPropertyChanged("CanExecute");
            }
        }

        public bool CanCancel
        {
            get
            {
                return _canCancel;
            }
            set
            {
                _canCancel = value;
                OnPropertyChanged("CanCancel");
            }
        }

        public int MaxAnchorCount
        {
            get
            {
                return _maxAnchorCount;
            }
            set
            {
                _maxAnchorCount = value;
                OnPropertyChanged("MaxAnchorCount");
            }
        }

        public ObservableCollection<QuipuAnchorItem> QuipuAnchorItems
        {
            get
            {
                if (quipuAnchorItems != null && quipuAnchorItems.Any())
                {
                    return quipuAnchorItems;
                }
                //для вёрстки ItemsControl
                else
                {
                    return InitItemCollection(new List<string>() { "https://en.wikipedia.org/wiki/SS_Newton_D._Baker", "https://en.wikipedia.org/wiki/Dumrawan", "https://en.wikipedia.org/wiki/American_Foreign_Policy_Council", "https://en.wikipedia.org/wiki/Heteropaussus", "https://en.wikipedia.org/wiki/Nathaniel_Gould_(1661–1728)", "https://en.wikipedia.org/wiki/Atzala", "https://en.wikipedia.org/wiki/Uladzimir_Sodal", "https://en.wikipedia.org/wiki/Judaean_Mountains", "https://en.wikipedia.org/wiki/Oriental_(municipality)", "https://en.wikipedia.org/wiki/Leave_the_World_Behind_(film)", "https://en.wikipedia.org/wiki/White_Wolves_II:_Legend_of_the_Wild", "https://en.wikipedia.org/wiki/2013–14_Scottish_League_Two", "https://en.wikipedia.org/wiki/Philosophy_of_chemistry", "https://en.wikipedia.org/wiki/Kulidzha", "https://en.wikipedia.org/wiki/Nissan_S30", "https://en.wikipedia.org/wiki/Okay_My_Gay", "https://en.wikipedia.org/wiki/2017_Ecuador_Open_Quito_–_Singles", "https://en.wikipedia.org/wiki/Sparse_network", "https://en.wikipedia.org/wiki/Engenheiro_Paulo_de_Frontin,_Rio_de_Janeiro", "https://en.wikipedia.org/wiki/Laz_Alonso", "https://en.wikipedia.org/wiki/Arthroconidium", "https://en.wikipedia.org/wiki/Viktor_Zykov", "https://en.wikipedia.org/wiki/Holy_Week_in_Santa_Cruz_de_La_Palma", "https://en.wikipedia.org/wiki/Martania_taiwana", "https://en.wikipedia.org/wiki/WWF_Hong_Kong", "https://en.wikipedia.org/wiki/Jason_Leland_Adams", "https://en.wikipedia.org/wiki/Cuttack–Sambalpur_line", "https://en.wikipedia.org/wiki/Mesila_Doda", "https://en.wikipedia.org/wiki/Johnatan_Cardoso", "https://en.wikipedia.org/wiki/Janne_Moilanen", "https://en.wikipedia.org/wiki/Cinema_of_Gabon", "https://en.wikipedia.org/wiki/Green_Island_(Wisconsin)", "https://en.wikipedia.org/wiki/Shoppers_Stop", "https://en.wikipedia.org/wiki/Bowerchalke", "https://en.wikipedia.org/wiki/Nifantovskaya", "https://en.wikipedia.org/wiki/IEEE_Daniel_E._Noble_Award", "https://en.wikipedia.org/wiki/Kohlrabi", "https://en.wikipedia.org/wiki/List_of_Olympic_medalists_in_curling", "https://en.wikipedia.org/wiki/Silsesquioxane", "https://en.wikipedia.org/wiki/Yangazino", "https://en.wikipedia.org/wiki/Steve_Patterson_(sports_executive)", "https://en.wikipedia.org/wiki/Sarika", "https://en.wikipedia.org/wiki/Nikolay_Brusentsov", "https://en.wikipedia.org/wiki/Duncote" });
                }
            }
            set
            {
                quipuAnchorItems = value;
                OnPropertyChanged("QuipuAnchorItems");
                OnPropertyChanged("LinksCount");
            }
        }

        public string GeneralState
        {
            get
            {
                return _generalState;
            }
            set
            {
                if (_generalState != value)
                {
                    _generalState = value;
                    OnPropertyChanged("GeneralState");
                }
            }
        }

        public int CheckedLinksCount
        {
            get
            {
                return _checkedLinksCount;
            }
            set
            {
                if (_checkedLinksCount != value)
                {
                    _checkedLinksCount = value;
                    OnPropertyChanged("CheckedLinksCount");
                }
            }
        }

        public int LinksCount
        {
            get
            {
                return QuipuAnchorItems!=null&& QuipuAnchorItems.Any()?QuipuAnchorItems.Count:0; 
            }
        }

        #region Commands
        public ICommand InitAppCommand
        {
            get
            {
                return _initAppCommand;
            }
            set
            {
                _initAppCommand = value;
            }
        }
        public ICommand ReadLinksFromConfigCommand
        {
            get
            {
                return _readLinksFromConfigCommand;
            }
            set
            {
                _readLinksFromConfigCommand = value;
            }
        }

        public ICommand UpdateConfigCommand
        {
            get
            {
                return _updateConfigCommand;
            }
            set
            {
                _updateConfigCommand = value;
            }
        }
        public ICommand StartCountingCommand
        {
            get
            {
                return _startCountingCommand;
            }
            set
            {
                _startCountingCommand = value;
            }
        }
        public ICommand StopCountingCommand
        {
            get
            {
                return _stopCountingCommand;
            }
            set
            {
                _stopCountingCommand = value;
            }
        }

        public void InitApp(object obj)
        {
            ReadLinksFromConfig(obj);
        }

        public void UpdateConfig(object obj)
        {
            ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "AnchorCounter.config" };
            Configuration configFile = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            int linksCount = 100;
            int.TryParse(settings["LinksCount"].Value,out linksCount);
            Task task = new Task(() =>
            {
                MaxAnchorCount = 0;
                GeneralState = "Getting random links from Wiki...";
                ChangeCanExecute();
                Process proc = new Process();
                proc.StartInfo.FileName = "CMD.exe";
                proc.StartInfo.Arguments = $"/c WikiGetRandomLink AnchorCounter.config {linksCount}";
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit();
                ChangeCanExecute();
                ReadLinksFromConfig(obj);
                GeneralState = "Ready!";
            });
            task.Start();
        }

        public void ReadLinksFromConfig(object obj)
        {
            Task task = new Task(() =>
            {
                MaxAnchorCount = 0;
                GeneralState = "Load data...";
                ChangeCanExecute();
                try
                {
                    if (File.Exists("AnchorCounter.config"))
                    {
                        ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = "AnchorCounter.config" };
                        Configuration configFile = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                        var settings = configFile.AppSettings.Settings;
                        QuipuAnchorItems = InitItemCollection(settings["URLS"].Value.Split(' ').ToList());
                    }
                }
                catch (ConfigurationErrorsException)
                {
                    Console.WriteLine("Error writing app settings");
                }
                ChangeCanExecute();
                GeneralState = "Ready!";
            });
            task.Start();
        }

        public void StartCounting(object obj)
        {
            ParallelOptions po = new ParallelOptions();
            cts = new CancellationTokenSource();
            po.CancellationToken = cts.Token;
            po.MaxDegreeOfParallelism = Convert.ToInt32(Math.Ceiling((Environment.ProcessorCount * 0.75) * 2.0));
            Task task = new Task(() =>
            {
                MaxAnchorCount = 0;
                GeneralState = "Counting...";
                ChangeCanExecute();
                ChangeCanCancel();
                var uries = new ConcurrentBag<QuipuAnchorItem>(QuipuAnchorItems);

                try
                {
                    Parallel.ForEach(QuipuAnchorItems, po, item =>
                    {
                        CheckedLinksCount++;
                        item.State = AnchorItemState.Pending;
                        if (po.CancellationToken.IsCancellationRequested)
                        {
                            item.State = AnchorItemState.Canceled;
                            return;
                        }

                        HtmlDocument doc = new HtmlDocument();
                        try
                        {
                            var url = item.Link;
                            var web = new HtmlWeb();
                            doc = web.Load(url);
                            item.State = AnchorItemState.OK;
                            if (po.CancellationToken.IsCancellationRequested)
                            {
                                item.State = AnchorItemState.Canceled;
                                return;
                            }
                        }
                        catch
                        {
                            item.State = AnchorItemState.BadResponse;
                            if (po.CancellationToken.IsCancellationRequested)
                            {
                                item.State = AnchorItemState.Canceled;
                                return;
                            }
                        }

                        try
                        {
                            item.Title = doc.DocumentNode.SelectSingleNode("//title").InnerText.Replace(" - Wikipedia", "");
                            item.AnchorCount = doc.DocumentNode.SelectNodes("//a").Count;
                            item.Content = doc.Text;
                            item.State = AnchorItemState.Counted;
                            MaxAnchorCount = MaxAnchorCount > item.AnchorCount ? MaxAnchorCount : item.AnchorCount;
                        }
                        catch
                        {
                            item.State = AnchorItemState.BadResponse;
                        }
                        if (po.CancellationToken.IsCancellationRequested)
                        {
                            item.State = AnchorItemState.Canceled;
                            return;
                        }
                    });
                    ChangeCanExecute();
                    ChangeCanCancel();
                }
                catch (OperationCanceledException)
                {
                }
                finally
                {
                    cts.Dispose();
                }
                GeneralState = "Ready!";
            });
            task.Start();
        }

        public void StopCounting(object obj)
        {
            cts.Cancel();
            CanExecute = true;
            CanCancel = false;
            CheckedLinksCount = 0;
        }

        public void ChangeCanExecute()
        {
            CanExecute = !CanExecute;
            CheckedLinksCount = 0;
        }

        public void ChangeCanCancel()
        {
            CanCancel = !CanCancel;
            CheckedLinksCount = 0;
        }
        #endregion

        ObservableCollection<QuipuAnchorItem> InitItemCollection(List<string> links)
        {
            ObservableCollection<QuipuAnchorItem> result = new ObservableCollection<QuipuAnchorItem>();
            foreach (string link in links)
            {
                result.Add(new QuipuAnchorItem()
                {
                    AnchorCount = 0,
                    Content = string.Empty,
                    Id = Guid.NewGuid(),
                    Link = link,
                    State = AnchorItemState.None,
                    Title = link
                });
            }
            return result;
        }
    }
}
