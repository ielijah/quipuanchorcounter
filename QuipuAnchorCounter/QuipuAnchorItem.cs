﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuipuAnchorCounter
{
    class QuipuAnchorItem : INotifyPropertyChanged
    {
        Guid _id;
        string _title;
        string _link;
        AnchorItemState _state = AnchorItemState.None;
        int _anchorCount;
        string _content;

        #region Properties
        public Guid Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    OnPropertyChanged("Title");
                }
            }
        }
        public string Link
        {
            get
            {
                return _link;
            }
            set
            {
                if (_link != value)
                {
                    _link = value;
                    OnPropertyChanged("Link");
                }
            }
        }
        public AnchorItemState State
        {
            get
            {
                return _state;
            }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    OnPropertyChanged("State");
                }
            }
        }
        public int AnchorCount
        {
            get
            {
                return _anchorCount;
            }
            set
            {
                if (_anchorCount != value)
                {
                    _anchorCount = value;
                    OnPropertyChanged("AnchorCount");
                }
            }
        }
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                if (_content != value)
                {
                    _content = value;
                    OnPropertyChanged("Content");
                }
            }
        }
        #endregion
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    enum AnchorItemState
    {
        None,
        Pending,
        OK,
        Counted,
        Canceled,
        BadResponse
    }
}
