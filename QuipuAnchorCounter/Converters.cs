﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace QuipuAnchorCounter
{
    class AnchorItemState2StringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch (value.ToString().ToUpper())
            {
                case "NONE":
                    return "Not counted";
                case "PENDING":
                    return "HTML pending";
                case "OK":
                    return "Recieved";
                case "COUNTED":
                    return "Links counted";
                case "CANCELED":
                    return "Canceled";
                case "BADRESPONSE":
                    return "Error";
                default:
                    return "Not counted";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class Bool2VisibleOrHidden : IValueConverter
    {
        #region Constructors
        /// <summary>
        /// The default constructor
        /// </summary>
        public Bool2VisibleOrHidden() { }
        #endregion

        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            bool bValue = (bool)value;
            if (bValue == (bool)(parameter ?? true))
                return Visibility.Visible;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;

            if (visibility == Visibility.Visible)
                return true == (bool)(parameter ?? true);
            else
                return false == (bool)(parameter ?? true);
        }
        #endregion
    }

    public class AnchorCount2BrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values.Any() && values.Length > 1 && (int)values[0] == (int)values[1])
                return Brushes.Green;
            else
                return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
